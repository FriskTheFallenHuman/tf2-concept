@echo off
setlocal

rem Compile only a short set of deferred shaders
set deferred_all_shaders=0

rem == Setup path to nmake.exe, from vc 2005 common tools directory ==
call "%VS120COMNTOOLS%vsvars32.bat"

:: transforms GAMEDIR from relative to absolute path (if it is not already).
if %ABBR_PATHS% equ 1 goto skip_path_fix
set _temp=%CD%
cd %GAMEDIR%
set GAMEDIR=%CD%
cd %_temp%
:skip_path_fix

set TTEXE=..\..\devtools\bin\timeprecise.exe
if not exist %TTEXE% goto no_ttexe
goto no_ttexe_end

:no_ttexe
set TTEXE=time /t
:no_ttexe_end

rem echo.
rem echo ~~~~~~ buildsdkshaders %* ~~~~~~
%TTEXE% -cur-Q
set tt_all_start=%ERRORLEVEL%
set tt_all_chkpt=%tt_start%

set BUILD_SHADER=call buildshaders.bat
set ARG_EXTRA=

rem echo.
if not "%deferred_all_shaders%" == "1" (
  %BUILD_SHADER% deferred_shaders 		-game %GAMEDIR% -source %SOURCEDIR% -dx9_30 -force30
) else (
  %BUILD_SHADER% deferred_shaders_short -game %GAMEDIR% -source %SOURCEDIR% -dx9_30 -force30
)