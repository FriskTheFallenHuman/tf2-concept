@echo off
setlocal

rem ================================
rem ==== MOD PATH CONFIGURATIONS ===

rem == Set the absolute or relative path to your mod's game directory here ==
rem == if ABBR_PATHS is 1 you should make this an absolute path
set GAMEDIR=..\..\..\game\hl2

rem == Set the relative path to steamapps\common\Alien Swarm\bin ==
rem == As above, this path originally did not support long directory names or spaces ==
set SDKBINDIR=..\..\..\game\bin

rem ==  Set the Path to your mods root source code ==
rem this should already be correct, accepts relative paths only!
set SOURCEDIR=..\..

rem == Originally GAMEDIR and SDKBINDIR did not support long file/directory names, so if you have ==
rem == issues, then set ABBR_PATHS here to 1 and update the paths below: ==
rem == instead of a path such as "C:\Program Files\Steam\steamapps\mymod" ==
rem == you may need to find the 8.3 abbreviation for the directory name using 'dir /x' ==
rem == and set the directory to something like C:\PROGRA~2\Steam\steamapps\sourcemods\mymod ==
set ABBR_PATHS=0

rem ==== MOD PATH CONFIGURATIONS END ===
rem ====================================

call buildsdkshaders.bat