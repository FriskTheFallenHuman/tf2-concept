#ifndef BACKGROUNDVIDEO_H
#define BACKGROUNDVIDEO_H
#ifdef _WIN32
#pragma once
#endif

#include "avi/ibik.h"

class HUDVideoPanel;

class CBackgroundMovie
{
public:
	CBackgroundMovie();
	~CBackgroundMovie();

	void Update();
	void SetCurrentMovie( const char *szFilename );
	int SetTextureMaterial();
	void ClearCurrentMovie();
 
private:
	BIKMaterial_t m_nBIKMaterial;
	int m_nTextureID;
	char m_szCurrentMovie[ MAX_PATH ];
	int m_nLastGameState;
};

CBackgroundMovie	*BackgroundMovie();

#endif // BACKGROUNDVIDEO_H
