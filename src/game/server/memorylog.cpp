//========= Copyright � 1996-2008, Valve Corporation, All rights reserved. ============//
//
// Purpose:	See memorylog.h
//
//=============================================================================//
#include "cbase.h"
#include "memorylog.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

// Memory log auto game system instantiation
CMemoryLog g_MemoryLog;

void CMemoryLog::LevelInitPostEntity( void )
{
}